//
//  FeedbackViewController.swift
//  Youni
//
//  Created by Aurelio Aliberti on 27/03/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit



class FeedbackViewController: UIViewController{
    
    
    @IBOutlet weak var weeksForWrittenErrorLbl: UILabel!
    @IBOutlet weak var weeksForOraErrorlLbl: UILabel!
    @IBOutlet weak var examGradeErrorLbl: UILabel!
    
    var subject = ""
    var professor = ""
    var cfu = 0
    var cdl = ""
    
    var writtenExamVoted = 0
    var oralExamVoted = 0
    
    var keyboardHeight : CGFloat = 0.0
    
    var textFieldViewIsHidden = true
    
    @IBOutlet weak var cdlTextField: UITextField!
    @IBOutlet weak var examTextField: UITextField!
    @IBOutlet weak var professorTextField: UITextField!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var cdlLbl: UILabel!
    @IBOutlet weak var professorLbl: UILabel!
    @IBOutlet weak var cfuNumberLbl: UILabel!
    
    
    @IBOutlet weak var minTwoWrittenExamDifficultBtn: UIButton!
    @IBOutlet weak var minOneWrittenExamDifficultBtn: UIButton!
    @IBOutlet weak var zeroWrittenExamDifficultBtn: UIButton!
    @IBOutlet weak var plusOneWrittenExamDifficultBtn: UIButton!
    @IBOutlet weak var plusTwoWrittenExamDifficultBtn: UIButton!
    
    @IBOutlet weak var minTwoOralExamDifficultBtn: UIButton!
    @IBOutlet weak var minOneOralExamDifficultBtn: UIButton!
    @IBOutlet weak var zeroOralExamDifficultBtn: UIButton!
    @IBOutlet weak var plusOneOralExamDifficultBtn: UIButton!
    @IBOutlet weak var plusTwoOralExamDifficultBtn: UIButton!
    
    
    
    @IBOutlet weak var writtenTextField: UITextField!
    @IBOutlet weak var oralTextField: UITextField!
    
    @IBOutlet weak var voteTextField: UITextField!
    @IBOutlet weak var attemptsTextField: UITextField!
    @IBOutlet weak var experienceTextView: UITextView!
    
    var checkWrittenDifficult = false
    var checkOralDifficult = false
    
    let minusTwoEmptyEmoji = UIImage(named: "empty_minus_two")
    let minusOneEmptyEmoji = UIImage(named: "empty_minus_one")
    let zeroEmptyEmoji = UIImage(named: "empty_zero")
    let plusOneEmptyEmoji = UIImage(named: "empty_plus_one")
    let plusTwoEmptyEmoji = UIImage(named: "empty_plus_two")
    
    let minusTwoFullEmoji = UIImage(named: "filled_minus_two")
    let minusOneFullEmoji = UIImage(named: "filled_minus_one")
    let zeroFullEmoji = UIImage(named: "filled_zero")
    let plusOneFullEmoji = UIImage(named: "filled_plus_one")
    let plusTwoFullEmoji = UIImage(named: "filled_plus_two")
    
    var firstTime : Bool = false
    var firstTimeAlert = true
    
    @IBOutlet weak var TextFieldView: UIView!
    
    let confirmAlert = UIAlertController(title: "Grazie!", message: "La tua opinione è importante.", preferredStyle: UIAlertController.Style.alert)
    let emptyFieldsAlert = UIAlertController(title: "Aspè" , message: "Compila almeno una scheda tra 'Il mio esame' e 'La tua opinione.'" , preferredStyle: UIAlertController.Style.alert)
    let textCountAlert = UIAlertController(title: "Aspè" , message: "Inserisci almeno 20 caratteri." , preferredStyle: UIAlertController.Style.alert)
    let minAndMaxAlert = UIAlertController(title: "Aspè" , message: "Inserisci un valore tra 1 e 10 settimane." , preferredStyle: UIAlertController.Style.alert)
    let examVotetAlert = UIAlertController(title: "Aspè" , message: "Inserisci un voto tra 18 e 30." , preferredStyle: UIAlertController.Style.alert)
    let infoTeacherAlert = UIAlertController(title: "Aspè" , message: "Controlla i campi CDL,Esame e Professore." , preferredStyle: UIAlertController.Style.alert)
    let genericErrorAlert = UIAlertController(title:"Aspè", message: "Controlla i campi indicati", preferredStyle: UIAlertController.Style.alert)
    
    var weeksForPreparingWritten : Int = 0
    var weeksForPreparingOral : Int = 0
    var examGrade : Int = 0
    var examAttempt : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weeksForOraErrorlLbl.isHidden = true
        weeksForWrittenErrorLbl.isHidden = true
        examGradeErrorLbl.isHidden = true
        
        TextFieldView.isHidden = textFieldViewIsHidden
        subjectLbl.text = subject
        professorLbl.text = professor
        cdlLbl.text = cdl
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(FeedbackViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.keyboardWillChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        //setBackgroundButtons()
        setEmojiContainers()
        setEmptyEmojiInView(in: emojiButtonsForWrittenContainer)
        setEmptyEmojiInView(in: emojiButtonsForOralContainer)
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    
    enum errorType: Int{
        
        case weeksForWritten
        case weeksforOral
        case examGrade
        }
    
    @IBAction func weeksForScritto(_ sender: Any) {
        
       firstTime = true
    }
    
    
    
    @IBAction func doneBtnTapped(_ sender: Any) {
        
//        let weeksForPreparingWritten = Int(writtenTextField.text!)
//        let weeksForPreparingOral = Int(oralTextField.text!)
//        let examGrade = Int(voteTextField.text!)
//        let examAttempt = Int(attemptsTextField.text!)
        
        weeksForWrittenErrorLbl.isHidden = true
        weeksForOraErrorlLbl.isHidden = true
        examGradeErrorLbl.isHidden = true
        
        if isSubjectNotFoundView(){
            
            if isInfoCardCorrectlyFilled() == false{
                
                presentErrorAlert(errorAlert: infoTeacherAlert)
            }
        }
        
        if isMyExamCorrectlyFilled(){
            
            presentConfirmAlert()
            //SEND TO SERVER MISSING
            
        }else if isMyOpinionCorrectlyFilled() {
            
            presentConfirmAlert()
            //SEND TO SERVER MISSING
            
        }else {
            
            if setErrorLabel(){
                presentErrorAlert(errorAlert: genericErrorAlert)
            }else{
                
                presentConfirmAlert()
                
            }
            
        }
    }
    
    
    
    func presentConfirmAlert(){
        
        if isSubjectNotFoundView(){
            
            confirmAlert.addAction(UIAlertAction(title: "Conferma", style: UIAlertAction.Style.default, handler: {action in
                self.performSegue(withIdentifier: "unwindSegueSubjects", sender: self)}))
            present(confirmAlert, animated:  true, completion: nil)
            
            
        }else{
        
        confirmAlert.addAction(UIAlertAction(title: "Conferma", style: UIAlertAction.Style.default, handler: {action in
            self.performSegue(withIdentifier: "unwindSegueFeedback", sender: self)}))
        present(confirmAlert, animated:  true, completion: nil)
        }
    }
    
    func presentErrorAlert(errorAlert: UIAlertController){
        
        if firstTimeAlert == true{
        errorAlert.addAction(UIAlertAction(title: "Riprova", style: UIAlertAction.Style.default))
        firstTimeAlert = false
        }
        
        present(errorAlert, animated:  true, completion: nil)
        
    }
    
    func setErrorLabel() -> Bool{
        
        weeksForPreparingWritten = Int(writtenTextField.text!) ?? 0
        weeksForPreparingOral = Int(oralTextField.text!) ?? 0
        examGrade = Int(voteTextField.text!) ?? 0
        examAttempt = Int(attemptsTextField.text!) ?? 0
        
        var found = false
        
        if checkWeeksRange(value: weeksForPreparingWritten) == false{
            
            print("")
            weeksForWrittenErrorLbl.isHidden = false
            found = true
            
        }
        
        if checkWeeksRange(value: weeksForPreparingOral) == false{
            
            weeksForOraErrorlLbl.isHidden = false
            found = true
            
        }
        
        if checkExamRange(value: examGrade) == false{
            
            examGradeErrorLbl.isHidden = false
            found = true
        }
        
        return found
        }
    
    
    
    func isMyExamFilled() -> Bool{
        
        if weeksForPreparingWritten != 0 && weeksForPreparingOral != 0 && examGrade != 0 && examAttempt != 0 {
            
            return true
        } else { return false }
    }
    
    func isMyExamCorrectlyFilled() -> Bool{
        
        if isMyExamFilled(){
            
            if checkWeeksRange(value: weeksForPreparingWritten) && checkWeeksRange(value: weeksForPreparingOral) && checkExamRange(value: examGrade){
               
                return true
            }
        }
        
        return false
       }
    
    func isMyOpinionCorrectlyFilled() -> Bool{
        
        if experienceTextView.text != "" && experienceTextView.text.count > 20{
            return true
            
        }else { return false }
    }
    
    func checkWeeksRange(value: Int) -> Bool{
        
        if value > 0 && value <= 10{
            return true
        }else { return false }
        
    }
    
    func checkExamRange(value: Int) -> Bool{
        
        if value >= 18 && value <= 30{
            return true
            
        }else { return false }
    }
    
    
    func isSubjectNotFoundView() -> Bool{
        
        if textFieldViewIsHidden == false {
            
            return true
            
        }else { return false }
    }
    
    func isInfoCardFilled() -> Bool{
        
        if cdlTextField.text != "" && professorTextField.text != "" && examTextField.text != "" {
            
            return true
        } else { return false }
    }
    
    
    func checkTextFieldsAreFilled() -> Bool{
        
        if writtenTextField.text != "" && oralTextField.text != "" && voteTextField.text != "" && attemptsTextField.text != "" && checkWrittenDifficult == true && checkOralDifficult == true {
            
            return true
            
        }else{ return false }
    }
    
    
    func isInfoCardCorrectlyFilled() -> Bool {
        
        if isInfoCardFilled() &&  (cdlTextField.text!.count > 5 || examTextField.text!.count > 5 || professorTextField.text!.count > 5){
            
            return true
        }
        else { return false }
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        
        keyboardHeight = keyboardFrame.height
        
        if firstTime != true {
            
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardFrame.height
            }
        }
        else {
            firstTime = false
        }
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y = 0
        }
    }
    
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            else {return}
        
        let keyboardFrame = keyboardSize.cgRectValue
        
        if firstTime != true {
            
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardFrame.height
            }
            else{
                keyboardHeight  = keyboardFrame.height - keyboardHeight
                self.view.frame.origin.y = self.view.frame.origin.y - keyboardHeight
            }
        }
    }
    
    
    var emojiButtonsForWrittenContainer = [UIButton]()
    var emojiButtonsForOralContainer = [UIButton]()
    var emojiEmptyImageContainer = [UIImage]()
    var emojiFullImageContainer = [UIImage]()
    
    let maxEmojiNumber = 5
    
    func setEmojiContainers(){
        
        for i in 0...maxEmojiNumber-1{
            
            let image = UIImage(named: "emptyEmoji\(i)")!
            emojiEmptyImageContainer.append(image)
        }
        
        for i in 0...maxEmojiNumber-1{
            
            let image = UIImage(named: "fullEmoji\(i)")
            emojiFullImageContainer.append(image!)
        }
        
        emojiButtonsForWrittenContainer.append(minTwoWrittenExamDifficultBtn)
        emojiButtonsForWrittenContainer.append(minOneWrittenExamDifficultBtn)
        emojiButtonsForWrittenContainer.append(zeroWrittenExamDifficultBtn)
        emojiButtonsForWrittenContainer.append(plusOneWrittenExamDifficultBtn)
        emojiButtonsForWrittenContainer.append(plusTwoWrittenExamDifficultBtn)
        
        emojiButtonsForOralContainer.append(minTwoOralExamDifficultBtn)
        emojiButtonsForOralContainer.append(minOneOralExamDifficultBtn)
        emojiButtonsForOralContainer.append(zeroOralExamDifficultBtn)
        emojiButtonsForOralContainer.append(plusOneOralExamDifficultBtn)
        emojiButtonsForOralContainer.append(plusTwoOralExamDifficultBtn)
    }
    //Set colored empty circles
    func setEmptyEmojiInView(in container: [UIButton]){
        
        for index in 0...container.count-1{
            
            container[index].setBackgroundImage(emojiEmptyImageContainer[index], for: .normal)
        }
    }
    //set smiling emoji on tapped circle, set to empty the others
    func makeEmojiSmile(in position: Int, for container: [UIButton]){
        
        setEmptyEmojiInView(in: container)
        container[position].setBackgroundImage(emojiFullImageContainer[position], for: .normal)
    }
    
    @IBAction func minTwoWrittenTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 0, for: emojiButtonsForWrittenContainer)
        checkWrittenDifficult = true
        writtenExamVoted = -2
    }
    
    @IBAction func minOneWrittenTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 1, for: emojiButtonsForWrittenContainer)
        checkWrittenDifficult = true
        writtenExamVoted = -1
    }
    
    @IBAction func zeroScrittoTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 2, for: emojiButtonsForWrittenContainer)
        checkWrittenDifficult = true
        writtenExamVoted = 0
    }
    
    @IBAction func plusOneScrittoTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 3, for: emojiButtonsForWrittenContainer)
        checkWrittenDifficult = true
        writtenExamVoted = 1
    }
    
    @IBAction func plusTwoScrittoTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 4, for: emojiButtonsForWrittenContainer)
        checkWrittenDifficult = true
        writtenExamVoted = 2
    }
    
    @IBAction func minTwoOraleTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 0, for: emojiButtonsForOralContainer)
        checkOralDifficult = true
        oralExamVoted = -2
    }
    
    @IBAction func minOneOraleTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 1, for: emojiButtonsForOralContainer)
        checkOralDifficult = true
        oralExamVoted = -1
    }
    
    @IBAction func zeroOraleTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 2, for: emojiButtonsForOralContainer)
        checkOralDifficult = true
        oralExamVoted = 0
    }
    
    @IBAction func plusOneOraleTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 3, for: emojiButtonsForOralContainer)
        checkOralDifficult = true
        oralExamVoted = 1
    }
    
    @IBAction func plusTwoOraleTapped(_ sender: Any) {
        
        makeEmojiSmile(in: 4, for: emojiButtonsForOralContainer)
        checkOralDifficult = true
        oralExamVoted = 2
    }
}


extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}



