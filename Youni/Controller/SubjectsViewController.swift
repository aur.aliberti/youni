//
//  SubjectsViewController.swift
//  Youni
//
//  Created by Aurelio Aliberti on 20/02/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit
import KituraKit

var subjects = [SubjectResponse]()
var currentSubjects = [SubjectResponse]()
var recentSubjects = [SubjectResponse]()
var favoriteSubjects = [SubjectResponse]()

var selectedItem = 0

class SubjectsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSearchBar()
        recentSubjects = loadSubjects(forKey: "recentSubjects")
        favoriteSubjects = loadSubjects(forKey: "favoriteSubjects")
        
        self.hideKeyboardWhenTappedAround()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchBar(searchBar, selectedScopeButtonIndexDidChange: searchBar.selectedScopeButtonIndex)
    }
    
    
    @IBAction func subjectNotFoundBtn(_ sender: Any) {
        
        let alert = UIAlertController(title: "Non trovi quello che cerchi?", message: "Aggiungi tu la prima esperienza.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Annulla", style: UIAlertAction.Style.default))
        
        alert.addAction(UIAlertAction(title: "Procedi", style: UIAlertAction.Style.default, handler: {
            
            action in
            
            self.performSegue(withIdentifier: "subjectNotFound", sender: self)
            
        }
        ))
        present(alert, animated:  true, completion: nil)
    }
    
    
    
    //***********+SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueSubjects" {
            
            if let destination = segue.destination as? InfoCardViewController{
                
                destination.subjectName = currentSubjects[selectedItem].subject_name
                destination.courseName = currentSubjects[selectedItem].course_name
                destination.idSubject = currentSubjects[selectedItem].subject_id
                destination.idCourse = currentSubjects[selectedItem].course_id
                }
        }
        
        if segue.identifier == "subjectNotFound" {
            
            if let destination = segue.destination as? FeedbackViewController{
                
                destination.textFieldViewIsHidden = false
            }
        }
    }
    
    //**************TABLEVIEW MATERIE************
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSubjects.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectTableViewCell", for: indexPath) as! SubjectTableViewCell
        
        cell.subjectLabel.text = currentSubjects[indexPath.row].subject_name
        cell.courseLabel.text = currentSubjects[indexPath.row].course_name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedItem = indexPath.row
        
        let position = checkElementInArray(subjects: recentSubjects, subject: currentSubjects[selectedItem])
        
        if position == -1 {
            
            recentSubjects.insert(currentSubjects[selectedItem], at: 0)
            
        }else {
            
            recentSubjects.remove(at: position)
            recentSubjects.insert(currentSubjects[selectedItem], at: 0)
            
        }
        saveSubjects(subjects: recentSubjects, forKey: "recentSubjects")
        performSegue(withIdentifier: "segueSubjects", sender: self)
    }
    
    //*****************SEARCH BAR***************
    
    private func setSearchBar(){
        searchBar.delegate = self
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //fare tutti i controlli, ora c'è solo il controllo sull'Uppercase
        guard !searchText.isEmpty else {
            
            currentSubjects = subjects
            tableView.reloadData()
            return
        }
        
        currentSubjects = subjects.filter({ subject -> Bool in
            subject.subject_name.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    
    //Scope Button tableview change
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        switch selectedScope{
            
        case 0:
            currentSubjects = subjects
            
        case 1:
            currentSubjects = recentSubjects
            
        case 2:
            currentSubjects = favoriteSubjects
            
        default:
            break
        }
        
        tableView.reloadData()
    }
    
    
    @IBAction func unwindToSubjectViewController(segue: UIStoryboardSegue) {
        
        
        
    }
}


extension UIViewController {
    
    
    
    func checkElementInArray(subjects: [SubjectResponse], subject: SubjectResponse) -> Int {
        
        let count = subjects.count
        var i = 0
        
        while( i < count ) {
            
            if subjects[i].subject_name == subject.subject_name && subjects[i].course_name == subject.course_name {
                
                return i
                
            }else{
                i = i + 1
            }
        }
        return -1
    }
    
    
    func saveSubjects(subjects: [SubjectResponse], forKey: String){
        
        let placesData = try! JSONEncoder().encode(subjects)
        UserDefaults.standard.set(placesData, forKey: forKey)
        
        print("Ho salvato:")
        
        for subject in subjects {
            
            print(subject.subject_name)
        }
        print("\n")
    }
    
    
    func loadSubjects(forKey: String) -> [SubjectResponse]{
        
        if UserDefaults.standard.data(forKey: forKey) != nil {
            
            let placeData = UserDefaults.standard.data(forKey: forKey)
            let placeArray = try! JSONDecoder().decode([SubjectResponse].self, from: placeData!)
            
            print("Array caricato:")
            for subject in placeArray{
                
                print(subject.subject_name + " " )
            }
            print("\n")
            
            return placeArray
            
        }else {
            
            return []
        }
    }
    
    
}


