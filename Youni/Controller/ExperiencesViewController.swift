//
//  ExperiencesViewController.swift
//  Youni
//
//  Created by Aurelio Aliberti on 21/05/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit

class ExperiencesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var experienceTableView: UITableView!
    @IBOutlet weak var experienceCollView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = experienceCollView.dequeueReusableCell(withReuseIdentifier: "experienceCell", for: indexPath)
        
        return cell
    }
    
    @IBAction func insertExperienceBtn(_ sender: Any) {
        
        self.performSegue(withIdentifier: "InsertNewExperience", sender: self)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSubjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = experienceTableView.dequeueReusableCell(withIdentifier: "experienceTableViewCell", for: indexPath) as! SubjectTableViewCell
        
        cell.subjectLabel.text = currentSubjects[indexPath.row].subject_name
        cell.courseLabel.text = currentSubjects[indexPath.row].course_name
        
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "insertNewExperience" {
            
            if let destination = segue.destination as? FeedbackViewController{
                
                destination.textFieldViewIsHidden = false
                
            }
        }
    }
  

}
