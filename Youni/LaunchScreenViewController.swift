//
//  LaunchScreenViewController.swift
//  CustomTableView
//
//  Created by PC on 07/03/2019.
//  Copyright © 2019 Aurelio Aliberti. All rights reserved.
//

import UIKit
import ImageIO
import KituraKit


class LaunchScreenViewController: UIViewController {
    
    @IBOutlet weak var gif: UIImageView!
    
    var counter = 0.0
    var counter2 = 0.0
    var counterTimer = Timer()
    var counterstartvalue = 1.0
    
    
    func startcounter (){
        
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(decrement), userInfo: nil, repeats: true)
        
    }
    
    func startcounter2 (){
        
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(decrement2), userInfo: nil, repeats: true)
        
    }
    
    @objc func decrement2(){
        counter2 -= 1
        if counter2 == 0 {
            
            performSegue(withIdentifier: "firstScreen", sender: self)
        }
        }
   
    @objc func decrement(){
        if counter == 0 { return
        }
        else  { counter -= 1
    }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gif.loadGif(name: "loading_screen")
        self.counter = self.counterstartvalue
        self.startcounter()
        
        setSubjects {
            DispatchQueue.main.async {
                
                currentSubjects = subjects
                if self.counter <= 0 {
                    
                    self.performSegue(withIdentifier: "firstScreen", sender: self)
                }
                else {
                    self.counter2 = self.counter
                    self.startcounter2()
                    
                    }
                }
            }
    }

    private func setSubjects(completion: @escaping (() ->Void)){
        
        
        guard let client = KituraKit(baseURL: "https://arcane-ravine-81898.herokuapp.com") else {
            print("Error creating KituraKit client")
            return
        }
        client.get("/allSubject") { (resSubjects: [SubjectResponse]?, error: Error?) in
            if resSubjects != nil {
                
                subjects = resSubjects!
                completion()
                
            }
        }
        currentSubjects = subjects
    }
}
