//
//  Professor.swift
//  Application
//
//  Created by Pietro Paolo Ugliano on 31/01/2019.
//

import Foundation

//*********************************************
/*Risposta del server*/
struct SubjectResponse: Codable {
    var subject_id: String
    var subject_name: String
    var course_id: String
    var course_name: String
    
    init?() {
        self.subject_id = ""
        self.subject_name = ""
        self.course_id = ""
        self.course_name = ""
        }
    
    init?(subject_id: String, subject_name: String, course_id: String, course_name: String) {
        
        self.subject_id = subject_id
        self.subject_name = subject_name
        self.course_id = course_id
        self.course_name = course_name
        }
}
//***********************************************

//************************************************
/*Ricerca la subject selezionata nel didSelectRow*/
struct SearchSubject: Codable {
    var subject_id: String
    var course_id: String
    
    init?(subject_id: String, course_id: String) {
        
        guard !subject_id.isEmpty else {
            return nil
        }
        guard !course_id.isEmpty else {
            return nil
        }
        
        self.subject_id = subject_id
        self.course_id = course_id
        
    }
}
//*************************************************
    
//************************************************
/*Risposta materia dettagliata dal DB*/

struct SubjectDetailResponse: Codable {
    
        var id_course: String
        var id_subject: String
        var cfu: Int16
        var propedeuticita: String
        var anno_corso: String
        var difficulty_write: Float
        var difficulty_oral: Float
        var num_tentativi: Float
        var voto: Float
        var num_prof: Int16
        var temp_prep_write: Float
        var temp_prep_oral: Float
        
        init?(id_course: String, id_subject: String, cfu: Int16, propedeuticita: String, anno_corso: String, difficulty_write: Float, difficulty_oral: Float, num_tentativi: Float, voto: Float, num_prof: Int16, temp_prep_write: Float, temp_prep_oral: Float) {
            
            self.id_course = id_course
            self.id_subject = id_subject
            self.cfu = cfu
            self.propedeuticita = propedeuticita
            self.anno_corso = anno_corso
            self.difficulty_write = difficulty_write
            self.difficulty_oral = difficulty_oral
            self.num_tentativi = num_tentativi
            self.voto = voto
            self.num_prof = num_prof
            self.temp_prep_write = temp_prep_write
            self.temp_prep_oral = temp_prep_oral
            
        }
        
        init?() {
            self.id_course = ""
            self.id_subject = ""
            self.cfu = 0
            self.propedeuticita = ""
            self.anno_corso = ""
            self.difficulty_write = 0.0
            self.difficulty_oral = 0.0
            self.num_tentativi = 0
            self.voto = 0
            self.num_prof = 0
            self.temp_prep_write = 0
            self.temp_prep_oral = 0
        }
        
    }

//**************************************

//**************************************************
//Invia richiesta professore al server

struct SearchProf: Codable {
    var id_subject: String
    var id_course: String
    
    init?(id_subject: String, id_course: String) {
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_subject = id_subject
        self.id_course = id_course
    }
}

//****************************************************

//****************************************************
/*Risposta ricerca professore per materia e corso*/

struct ProfResponse: Codable {
    var id_prof: String
    var name: String
    var surname: String
    
    init?(id_prof: String, name: String, surname: String) {
        
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !name.isEmpty else {
            return nil
        }
        guard !surname.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.name = name
        self.surname = surname
        
    }
    
}

//*******************************************************

//*****************************************************
//Richiesta informazioni dettagliate singolo professore

struct SearchProfStats: Codable {
    var id_prof: String
    var id_subject: String
    var id_course: String
    
    init?(id_prof: String, id_subject: String, id_course: String) {
        
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
    }
}
//****************************************************

//******************************************************
//Risposta informazioni dettagliate singolo professore

struct ProfStatsResponse: Codable {
    var id_prof: String
    var id_subject: String
    var id_course: String
    var prep_time_write: Float
    var voto: Float
    var difficulty_write: Float
    var difficulty_oral: Float
    var num_tentativi: Float
    var orario_ricevimento: String
    var orario_lezione: String
    var count_votes: Int16
    var prep_time_oral: Float
    
    init?(id_prof: String, id_subject: String, id_course: String, prep_time_write: Float, voto: Float, difficulty_write: Float, difficulty_oral: Float, num_tentativi: Float, orario_ricevimento: String, orario_lezione: String, count_votes: Int16, prep_time_oral: Float) {
        
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.prep_time_write = prep_time_write
        self.voto = voto
        self.difficulty_write = difficulty_write
        self.difficulty_oral = difficulty_oral
        self.num_tentativi = num_tentativi
        self.orario_ricevimento = orario_ricevimento
        self.orario_lezione = orario_lezione
        self.count_votes = count_votes
        self.prep_time_oral = prep_time_oral
        
    }
    
    init?() {
        
        self.id_prof = ""
        self.id_subject = ""
        self.id_course = ""
        self.prep_time_write = 0.0
        self.voto = 0.0
        self.difficulty_write = 0.0
        self.difficulty_oral = 0.0
        self.num_tentativi = 0.0
        self.orario_ricevimento = ""
        self.orario_lezione = ""
        self.count_votes = 0
        self.prep_time_oral = 0.0
        
    }
    
}

//***************************************************



//***************************************************
/*REVIEWED UP TO THIS POINT*/
//***************************************************





struct Professor: Codable {
    
    var id_prof: String
    var name: String
    var surname: String
    
    init?(id_prof: String, name: String, surname: String) {
        
        guard !id_prof.isEmpty else {
            return nil
        }
        
        guard !name.isEmpty else {
            return nil
        }
        
        guard !surname.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.name = name
        self.surname = surname
        
    }
    
}

struct Subject: Codable {
    
    var id_subject: String
    var name: String
    
    init?(id_subject: String, name: String) {
        
        guard !id_subject.isEmpty else {
            return nil
        }
        
        guard !name.isEmpty else {
            return nil
        }
        
        self.id_subject = id_subject
        self.name = name
        
    }
}

struct Course: Codable {
    
    var id_course: String
    var name: String
    
    init?(id_course: String, name: String) {
        
        guard !id_course.isEmpty else {
            return nil
        }
        
        guard !name.isEmpty else {
            return nil
        }
        
        self.id_course = id_course
        self.name = name
        
    }
}

struct User: Codable {
    
    var username: String
    var password: String
    
    init?(username: String, password: String) {
        
        guard !username.isEmpty else {
            return nil
        }
        
        guard !password.isEmpty else {
            return nil
        }
        
        self.username = username
        self.password = password
        
    }
}

struct Question: Codable {
    var id_question: String
    var id_prof: String
    var id_subject: String
    var id_course: String
    var questionTxt: String
    var ranking: Float
    
    init?(id_question: String, id_prof: String, id_subject: String, id_course: String, questionTxt: String, ranking: Float) {
        
        guard !id_question.isEmpty else {
            return nil
        }
        
        guard !id_prof.isEmpty else {
            return nil
        }
        
        guard !id_subject.isEmpty else {
            return nil
        }
        
        guard !id_course.isEmpty else {
            return nil
        }
        
        guard !questionTxt.isEmpty else {
            return nil
        }
        
        self.id_question = id_question
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.questionTxt = questionTxt
        self.ranking = ranking
        
    }
}

    
    


struct ResultQueryQuestion: Codable {
    var username: String
    var id_question: String
    var id_prof: String
    var id_subject: String
    var id_course: String
    var question: String
    var ranking: Float
    
    
    init?(username: String, id_question: String, id_prof: String, id_subject: String, id_course: String, question: String, ranking: Float) {
        
        guard !username.isEmpty else {
            return nil
        }
        
        guard !id_question.isEmpty else {
            return nil
        }
        
        guard !id_prof.isEmpty else {
            return nil
        }
        
        guard !id_subject.isEmpty else {
            return nil
        }
        
        guard !id_course.isEmpty else {
            return nil
        }
        
        guard !question.isEmpty else {
            return nil
        }
        
        self.username = username
        self.id_question = id_question
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.question = question
        self.ranking = ranking
        
    }
        
}


    



struct SearchQuestions: Codable {
    var id_prof: String
    var id_subject: String
    var id_course: String
    
    init?(id_prof: String, id_subject: String, id_course: String) {
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
    }
}









struct ResultQueryStudentExperience: Codable {
    var username: String
    var experience: String
    var upvotes: Int16
    var downvotes: Int16
    
    init?(username: String, experience: String, upvotes: Int16, downvotes: Int16) {
        
        guard !username.isEmpty else {
            return nil
        }
        
        self.username = username
        self.experience = experience
        self.upvotes = upvotes
        self.downvotes = downvotes
        
    }
}

struct UpdateProfStats: Codable {
    var id_prof: String
    var id_subject: String
    var id_course: String
    var prep_time: Float
    var explane: Float
    var difficulty: Float
    var presence: Bool
    var prep_type: Float
    var saltappello: Bool
    var prove_intercorso: Bool
    var count_votes: Int32
    
    init?(id_prof: String, id_subject: String, id_course: String, prep_time: Float, explane: Float, difficulty: Float, presence: Bool, prep_type: Float, saltappello: Bool, prove_intercorso: Bool, count_votes: Int32) {
        
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.prep_time = prep_time
        self.explane = explane
        self.difficulty = difficulty
        self.presence = presence
        self.prep_type = prep_type
        self.saltappello = saltappello
        self.prove_intercorso = prove_intercorso
        self.count_votes = count_votes
        
    }
    
}

struct UpdateQuestion: Codable {
    var id_question: String
    var id_prof: String
    var id_subject: String
    var id_course: String
    var ranking: Float
    
    init?(id_question: String, id_prof: String, id_subject: String, id_course: String, ranking: Float) {
       
        guard !id_question.isEmpty else {
            return nil
        }
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.id_question = id_question
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.ranking = ranking
        
    }
}

struct InsertStudentExperience: Codable {
    var username: String
    var id_prof: String
    var id_subject: String
    var id_course: String
    var temp_prep_scritto: Int16
    var voto: Int16
    var tentativi: Int16
    var diff_orale: Int16
    var diff_scritto: Int16
    var upvotes: Int16
    var downvotes: Int16
    var experience: String
    var temp_prep_orale: Int16
    
    init?(username: String, id_prof: String, id_subject: String, id_course: String, temp_prep_scritto: Int16, voto: Int16, tentativi: Int16, diff_orale: Int16, diff_scritto: Int16, experience: String, temp_prep_orale: Int16) {
        guard !username.isEmpty else {
            return nil
        }
        guard !id_prof.isEmpty else {
            return nil
        }
        guard !id_subject.isEmpty else {
            return nil
        }
        guard !id_course.isEmpty else {
            return nil
        }
        
        self.username = username
        self.id_prof = id_prof
        self.id_subject = id_subject
        self.id_course = id_course
        self.temp_prep_scritto = temp_prep_scritto
        self.voto = voto
        self.tentativi = tentativi
        self.diff_orale = diff_orale
        self.diff_scritto = diff_scritto
        self.upvotes = 0
        self.downvotes = 0
        self.experience = experience
        self.temp_prep_orale = temp_prep_orale
        
    }
}

struct MediaStudentExperience: Codable {
    var tempo_prep_scritto_medio: Double
    var tempo_prep_orale_medio: Double
    var voto_medio: Double
    var tentativi_media: Double
    var diff_orale_medio: Double
    var diff_scritto_medio: Double
    var count_votes: Int64
    
    init?(tempo_prep_scritto_medio: Double, tempo_prep_orale_medio: Double, voto_medio: Double, tentativi_media: Double, diff_orale_medio: Double, diff_scritto_medio: Double, count_votes: Int64) {
        
        self.tempo_prep_scritto_medio = tempo_prep_scritto_medio
        self.tempo_prep_orale_medio = tempo_prep_orale_medio
        self.voto_medio = voto_medio
        self.tentativi_media = tentativi_media
        self.diff_orale_medio = diff_orale_medio
        self.diff_scritto_medio = diff_scritto_medio
        self.count_votes = count_votes
        
    }
    
}
