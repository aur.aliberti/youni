//
//  InfoCardViewController.swift
//  Youni
//
//  Created by Aurelio Aliberti on 08/03/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit
import KituraKit
import ImageIO

class InfoCardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
   
    
    var subjectName: String = ""
    var courseName : String = ""
    var idSubject : String = ""
    var idCourse : String = ""
    var avgWritten : Float = 0.0
    var avgOral : Float = 0.0
    
    var teacherBtnLbl = ""
    
    var subject = SubjectResponse()
    var resultSubject = SubjectDetailResponse()!
    var resultTeachers = [ProfResponse]()
    var resultTeacherStats = ProfStatsResponse()!
    
    @IBOutlet weak var loadingGif: UIImageView!
    @IBOutlet weak var cfuLbl: UILabel!
    @IBOutlet weak var courseYear: UILabel!
    @IBOutlet weak var propedeuticityLbl: UILabel!
    @IBOutlet weak var timeForPreparingWritten: UILabel!
    @IBOutlet weak var timeForPreparingOral: UILabel!
    @IBOutlet weak var avgVoteLbl: UILabel!
    @IBOutlet weak var attemptsLbl: UILabel!
    @IBOutlet weak var selectTeacherBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    
    @IBOutlet weak var cellViewFeedback: UICollectionView!
    @IBOutlet weak var provaScrittaImg: UIImageView!
    @IBOutlet weak var provaOraleImg: UIImageView!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    let selectTeacherAlert = UIAlertController(title: "Aspè", message: "Seleziona un professore", preferredStyle: UIAlertController.Style.alert
    )
    
    let minusTwoBar = UIImage(named: "bar-2")
    let minusOneBar = UIImage(named: "bar-1")
    let zeroBar = UIImage(named: "bar0")
    let plusOneBar = UIImage(named: "bar+1")
    let plusTwoBar = UIImage(named: "bar+2")
    let star = UIImage(named: "star")
    let fullStar = UIImage(named: "fullStar")
    
    var positionInFavorites = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //Initial loading gif, waiting for Server
        scrollView.alpha = 0
        loadingGif.loadGif(name: "loading_screen")
        redRectangle.isHidden = true
        
        //Build the subject object to use for searching it in favorites
        subject = SubjectResponse(subject_id: idSubject, subject_name: subjectName, course_id: idCourse, course_name: courseName)!
        //serch that subject in favorites, if it is there, remove and insert on top
        positionInFavorites = checkElementInArray(subjects: favoriteSubjects, subject: subject!)
        
        //set the favorite star
        if positionInFavorites == -1 {
            
            favoriteButton.setBackgroundImage(star, for: .normal)
            
        }else {
            
            favoriteButton.setBackgroundImage(fullStar, for: .normal)
            }
        
        
        let searchSubjectSelected = SearchSubject(subject_id: idSubject, course_id: idCourse)
        
        //send to server the subject, wait for the whole infocard data
        sendSelectedSubject(subjectSelected: searchSubjectSelected!) {
            
            print("Info Subject server request...")
            DispatchQueue.main.async {
                
                self.subjectLabel.text = self.subjectName
                self.courseLabel.text = self.courseName
                self.cfuLbl.text = String(self.resultSubject.cfu)
                self.courseYear.text = self.resultSubject.anno_corso
                self.propedeuticityLbl.text = String(self.resultSubject.propedeuticita)
                self.timeForPreparingOral.text = String(self.resultSubject.temp_prep_oral)
                self.timeForPreparingWritten.text = String(self.resultSubject.temp_prep_write)
                self.avgVoteLbl.text = String(self.resultSubject.voto)
                self.attemptsLbl.text = String(self.resultSubject.num_tentativi)
                self.avgWritten = self.resultSubject.difficulty_write
                self.avgOral = self.resultSubject.difficulty_oral
                self.setSmileImages()
                self.loadingGif.isHidden = true
                self.scrollView.alpha = 1
                print("Subject detail Response received")
            }
            
        }
        
        
        let searchProf = SearchProf(id_subject: idSubject, id_course: idCourse)
        
        sendProfessorsRequest(professorRequest: searchProf!){
            
            print("Teachers server request...")
            DispatchQueue.main.async {
                print("Teachers Response received")
            }
        }
    }
    
    
    @IBAction func favoriteButtonTapped(_ sender: Any) {
        
        if positionInFavorites == -1 {
            
            //insert in Favorite
           favoriteButton.setBackgroundImage(fullStar, for: .normal)
            favoriteSubjects.insert(subject!, at: 0)
            
            positionInFavorites = 0
            saveSubjects(subjects: favoriteSubjects, forKey: "favoriteSubjects")
            
        }else{
            
            //removed from Favorites
            favoriteSubjects.remove(at: positionInFavorites)
            favoriteButton.setBackgroundImage(star, for: .normal)
            positionInFavorites = -1
            saveSubjects(subjects: favoriteSubjects, forKey: "favoriteSubjects")
            }
    }
    
    
    @IBOutlet weak var redRectangle: UIImageView!
    
    @IBAction func tellYourExperienceBtn(_ sender: Any) {
        
        
        if self.selectTeacherBtn.titleLabel?.text == "Tutti" ||  self.selectTeacherBtn.titleLabel?.text == "Seleziona Professore"{
            
           redRectangle.isHidden = false
            
            selectTeacherAlert.addAction(UIAlertAction(title: "Riprova", style: UIAlertAction.Style.default))
            present(selectTeacherAlert, animated:  true, completion: nil)
            
        }else{
        
        redRectangle.isHidden = true
        performSegue(withIdentifier: "segueSubject", sender: self)
        
    }
    }
    
    
    private func sendSelectedSubject(subjectSelected: SearchSubject, completion: @escaping (() ->Void)){
        
        guard let client = KituraKit(baseURL: "https://arcane-ravine-81898.herokuapp.com") else {
            print("Error creating KituraKit client")
            return
        }
        
        client.post("/searchSubjectCourse", data: subjectSelected) { (resSubject: SubjectDetailResponse?, error: Error?) in
            if resSubject != nil {
            
                 self.resultSubject = resSubject!
                
                completion()
              
            }
        }
    }
    
    private func sendProfessorsRequest(professorRequest: SearchProf, completion: @escaping (() ->Void)){
        
        guard let client = KituraKit(baseURL: "https://arcane-ravine-81898.herokuapp.com") else {
            print("Error creating KituraKit client")
            return
        }
        
        client.post("/searchProf", data: professorRequest) { (resTeachers: [ProfResponse]?, error: Error?) in
            if resTeachers != nil {
                
                self.resultTeachers = resTeachers!
                
                completion()
                
            }
        }
    }
    
    
    private func sendProfessorStatsRequest(professorStatsRequest: SearchProfStats, completion: @escaping (() ->Void)){
        
        guard let client = KituraKit(baseURL: "https://arcane-ravine-81898.herokuapp.com") else {
            print("Error creating KituraKit client")
            return
        }
        
        client.post("/searchProfStats", data: professorStatsRequest) { (resTeacherStats: ProfStatsResponse?, error: Error?) in
            if resTeacherStats != nil {
                
                self.resultTeacherStats = resTeacherStats!
                
                completion()
                
            }
        }
    }
    
    
    
    @IBAction func selectTeacherAction(_ sender: Any) {
        
        redRectangle.isHidden = true
        scrollView.alpha = 0
        loadingGif.isHidden = false
        
        let optionMenu = UIAlertController(title: nil, message: "Seleziona Professore", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Annulla", style: .cancel)
       optionMenu.addAction(cancelAction)
        let generale = UIAlertAction(title: "Tutti", style: .default, handler: {
            action in
            
            self.selectTeacherBtn.titleLabel?.text = "Tutti"
            self.timeForPreparingOral.text = String(self.resultSubject.temp_prep_oral)
            self.timeForPreparingWritten.text = String(self.resultSubject.temp_prep_write)
            self.avgVoteLbl.text = String(self.resultSubject.voto)
            self.attemptsLbl.text = String(self.resultSubject.num_tentativi)
            self.avgWritten = self.resultSubject.difficulty_write
            self.avgOral = self.resultSubject.difficulty_oral
            self.loadingGif.isHidden = true
            self.scrollView.alpha = 1
            
        })
        optionMenu.addAction(generale)
        
        
        for teacher in resultTeachers{
            
            let prof = UIAlertAction(title: teacher.name + " " + teacher.surname, style: .default, handler: {
                action in
                    
                self.selectTeacherBtn.titleLabel?.text = teacher.name + " " + teacher.surname
                
                
                let searchProfStats = SearchProfStats(id_prof: teacher.id_prof, id_subject: self.idSubject  , id_course: self.idCourse)
                
                self.sendProfessorStatsRequest(professorStatsRequest: searchProfStats!){
                    
                    print("ProfessorStats Request")
                    DispatchQueue.main.async {
                        
                        self.timeForPreparingOral.text = String(self.resultTeacherStats.prep_time_oral)
                        self.timeForPreparingWritten.text = String(self.resultTeacherStats.prep_time_write)
                        self.avgVoteLbl.text = String(self.resultTeacherStats.voto)
                        self.attemptsLbl.text = String(self.resultTeacherStats.num_tentativi)
                        
                        self.avgWritten = self.resultTeacherStats.difficulty_write
                        self.avgOral = self.resultTeacherStats.difficulty_oral
                        
                        print(self.resultTeacherStats.difficulty_write)
                        print(self.resultTeacherStats.difficulty_oral)
                        
                        self.loadingGif.isHidden = true
                        self.scrollView.alpha = 1
                        print("ProfessorStats Response")
                    }
                    }
            })
                    optionMenu.addAction(prof)
            
        }
        
        
        self.present(optionMenu, animated: true, completion: nil)
        }
    
    
    func setSmileImages(){
        
        if avgWritten < -1.5 {
            
            provaScrittaImg.image = minusTwoBar
            
        }else if avgWritten >= -1.5 && avgWritten < -0.5 {
            
            provaScrittaImg.image = minusOneBar
            
        }else if avgWritten >= -0.5 && avgWritten <= 0.5 {
            
            provaScrittaImg.image = zeroBar
            
        }else if avgWritten > 0.5 && avgWritten <= 1.5{
            
            provaScrittaImg.image = plusOneBar
            
        }else if avgWritten > 1.5 {
            
            provaScrittaImg.image = plusTwoBar
            }
        
        
        if avgOral < -1.5 {
            
            provaOraleImg.image = minusTwoBar
            
        }else if avgOral >= -1.5 && avgOral < -0.5 {
            
            provaOraleImg.image = minusOneBar
            
        }else if avgOral >= -0.5 && avgOral <= 0.5 {
            
            provaOraleImg.image = zeroBar
            
        }else if avgOral > 0.5 && avgOral <= 1.5{
            
            provaOraleImg.image = plusOneBar
            
        }else if avgOral > 1.5 {
            
            provaOraleImg.image = plusTwoBar
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = cellViewFeedback.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        teacherBtnLbl = selectTeacherBtn.titleLabel!.text!
        
        if segue.identifier == "segueSubject" {
            
            if let destination = segue.destination as? FeedbackViewController{
                
                destination.subject = subjectName
                destination.professor = (selectTeacherBtn.titleLabel?.text)!
                destination.cdl = courseName
                
            }
        }
    }
    
    @IBAction func unwindToInfoViewController(segue: UIStoryboardSegue) {
        
        selectTeacherBtn.titleLabel?.text = teacherBtnLbl
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
       
    }
    
    
    
  
    
    
  
    
    
    
    

}
