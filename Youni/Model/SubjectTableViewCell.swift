//
//  SubjectTableViewCell.swift
//  Youni
//
//  Created by Aurelio Aliberti on 20/02/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {

    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
